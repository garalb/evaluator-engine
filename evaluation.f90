module env_type

  implicit none

  integer, parameter, public :: STR_LENGTH = 132
  integer, parameter, public :: wp = selected_real_kind(15,100)

  type, public :: env_entry_t
     character(len=STR_LENGTH) :: name  = "@null@"
     real(wp)                  :: value = -huge(1.0_wp)
     logical                   :: read_only = .false.
     logical                   :: deferred = .false.
     character(len=STR_LENGTH) :: string_def ="@null@"
  end type env_entry_t

  type, public :: eval_env_t
     integer :: n_entries = 0
     type(env_entry_t), dimension(:), pointer :: entry => null()
  end type eval_env_t

end module env_type
!
!
module fparser_interface
    interface
       subroutine evaluate(FuncStr,Env, value, status)
         use env_type, only: wp, eval_env_t
         CHARACTER (LEN=*),  INTENT(in) :: FuncStr   ! String to evaluate
         type(eval_env_t), intent(in)   :: Env       ! Evaluation environment
         real(wp), intent(out)          :: value     ! Value of evaluation
         integer, intent(out), optional :: status
       end subroutine evaluate
    end interface

    interface
      function EvalErrMsg (EvalErrType) RESULT (msg)
        integer,	   intent(in) :: EvalErrType
	CHARACTER (LEN=48)            :: msg
      end function EvalErrMsg
    end interface
end module fparser_interface

module m_eval_env
!
! Simple evaluation environment
! A collection of name-value pairs
! Some of the names are tagged as "constants"
! and cannot be modified.
!
! Alberto Garcia, July 2012

  use env_type, only: eval_env_t, env_entry_t, wp
  use fparser_interface, only: evaluate, EvalErrMsg

  implicit none

  public :: new_env, reset, add_to_eval_env
  public :: set_value, get_value_by_name
  public :: get_value_by_index, show_env
  public :: search_variable
  public :: add_to_eval_env_deferred

  ! re-export these to simplify downstream use
  public :: eval_env_t, wp, evaluate, EvalErrMsg

  private

CONTAINS

  subroutine new_env(e,n)
    type(eval_env_t), intent(inout) :: e
    integer, intent(in)           :: n

    call reset(e)
    allocate(e%entry(n))
  end subroutine new_env
  
  subroutine reset(e)
    type(eval_env_t), intent(inout) :: e

    if (associated(e%entry)) then
       deallocate(e%entry)
       nullify(e%entry)
    endif
    e%n_entries = 0
  end subroutine reset

  subroutine add_to_eval_env(e,name,value,read_only)
    type(eval_env_t), intent(inout) :: e
    character(len=*), intent(in)    :: name
    real(wp), intent(in)            :: value
    logical, intent(in), optional   :: read_only

    type(env_entry_t), pointer :: p
    integer                     :: loc

    loc = search_variable(e,name)
    if (loc == 0) then

       ! 'name' is not in the environment
       ! We add it

       if (size(e%entry) == e%n_entries) then
          call enlarge(e)
       endif
       e%n_entries = e%n_entries + 1
       p => e%entry(e%n_entries)
       p%name = name
       p%value = value
       if (present(read_only)) then
          p%read_only = read_only
       endif

    else
       call die("Attempt to re-add: " //trim(name) //". Use set_value")
    endif
  end subroutine add_to_eval_env
!
  subroutine add_to_eval_env_deferred(e,name,string)
    type(eval_env_t), intent(inout) :: e
    character(len=*), intent(in)    :: name
    character(len=*), intent(in)    :: string

    type(env_entry_t), pointer :: p
    integer                     :: loc

    loc = search_variable(e,name)
    if (loc == 0) then

       ! 'name' is not in the environment
       ! We add it

       if (size(e%entry) == e%n_entries) then
          call enlarge(e)
       endif
       e%n_entries = e%n_entries + 1
       p => e%entry(e%n_entries)
       p%name = name
       p%deferred = .true.
       p%string_def = string

    else
       call die("Attempt to re-add: " //trim(name))
    endif
  end subroutine add_to_eval_env_deferred

!-------------------------------------------------------
  subroutine set_value(e,name,value)
    type(eval_env_t), intent(inout) :: e
    character(len=*), intent(in)    :: name
    real(wp), intent(in)            :: value

    type(env_entry_t), pointer :: p
    integer                     :: loc

    loc = search_variable(e,name)
    if (loc == 0) then

       ! 'name' is not in the environment
       call die("Trying to set value of unknown symbol: " //trim(name))
    else

       p => e%entry(loc)
       if (p%read_only) then
          call die("Attempt to reset constant: " //trim(name))
       else if (p%deferred) then
          call die("Attempt to reset deferred variable: " //trim(name))
       else
          p%value = value
       endif
    endif
  end subroutine set_value
!-------------------------------------------------------
  function get_value_by_name(e,name) result(value)
    type(eval_env_t), intent(in)    :: e
    character(len=*), intent(in)    :: name
    real(wp)                        :: value

    type(env_entry_t), pointer :: p
    integer                     :: loc

    loc = search_variable(e,name)
    if (loc == 0) then
       ! 'name' is not in the environment
       call die("Tried to get value of unknown symbol: " //trim(name))
    else
       p => e%entry(loc)
       if (p%deferred) then
          call evaluate(p%string_def, e, value)
       else
          value = p%value 
       endif
    endif
  end function get_value_by_name
!---------------------------------------------------
  function get_value_by_index(e,loc) result(value)
    type(eval_env_t), intent(in)    :: e
    integer, intent(in)             :: loc
    real(wp)                        :: value

    type(env_entry_t), pointer :: p

    if (loc > e%n_entries) then
       ! 'name' is not in the environment
       call die("Tried to get value at invalid index")
    else
       p => e%entry(loc)
       if (p%deferred) then
          call evaluate(p%string_def, e, value)
       else
          value = p%value 
       endif
    endif
  end function get_value_by_index
!-------------------------------------------------------
  subroutine show_env(e)
    type(eval_env_t), intent(inout) :: e

    integer :: i
    type(env_entry_t), pointer :: p
    real(wp)                   :: value

    print "(a)", "--- current evaluation environment"
    do i = 1, e%n_entries
       p => e%entry(i)
       if (p%read_only) then
          print "(a24,f20.10,2x,a)", trim(p%name), p%value, "constant"
       else if (p%deferred) then
          call evaluate(p%string_def, e, value)
          print "(a24,f20.10,2x,a)", trim(p%name), &
                                   value, "(" // trim(p%string_def) // ")"
       else
          print "(a24,f20.10)", trim(p%name), p%value
       endif
    enddo
  end subroutine show_env

  function search_variable(e,name) result(loc)
    type(eval_env_t), intent(in) :: e
    character(len=*), intent(in) :: name
    integer                      :: loc

    integer :: i

    loc = 0
    do i = 1, e%n_entries
       if (leqi(e%entry(i)%name,name)) then
          loc = i
          return
       endif
    enddo
  end function search_variable

!---Auxiliary routines-----------------------------------------
!
  subroutine enlarge(e)
    type(eval_env_t), intent(inout) :: e

    type(env_entry_t), allocatable :: tmp(:)
    
    integer :: i, nold

    nold = e%n_entries

    allocate(tmp(nold))
    do i = 1, e%n_entries
       tmp(i) = e%entry(i)
    enddo
    deallocate(e%entry)
    allocate(e%entry(ceiling(1.6*nold)))
    do i = 1, nold
       e%entry(i) = tmp(i)
    enddo

    deallocate(tmp)

  end subroutine enlarge

  logical function leqi(a,b)
    character(len=*), intent(in) :: a, b
    leqi = (a == b)
  end function leqi

  subroutine die(str)
    character(len=*), intent(in) :: str

    print *, str
    stop
  end subroutine die
end module m_eval_env
