PROGRAM repl

  use parse
  use m_eval_env

  IMPLICIT NONE

  integer, parameter :: dp = selected_real_kind(15,100)

  REAL(dp)          :: value
  integer :: status, ieq

  character(len=128) line, name, def_string
  type(parsed_line), pointer :: p
  type(eval_env_t)  :: env

  !
  call new_env(env,3)

  DO
     read(5,fmt="(a)",end=999) line
     p=>digest(line)
     if (search(p,"variable",ind=ieq)) then
        if ((ieq == 1) .and. match(p,"nnv")) then
           name = names(p,2)
           value = values(p,1)
           call add_to_eval_env(env,trim(name),value)
        else
           call die("Syntax error in variable line")
        endif
     else if (search(p,"constant",ind=ieq)) then
        if ((ieq == 1) .and. match(p,"nnv")) then
           name = names(p,2)
           value = values(p,1)
           call add_to_eval_env(env,trim(name),value,read_only=.true.)
        else
           call die("Syntax error in constant line")
        endif
     else if (search(p,"constraint",ind=ieq)) then
        if ((ieq == 1) .and. match(p,"nnn")) then
           name = names(p,2)
           def_string = names(p,3)
           call add_to_eval_env_deferred(env,trim(name),trim(def_string))
        else
           call die("Syntax error in constraint line")
        endif
     else if (search(p,"set",ind=ieq)) then
        if ((ieq == 1) .and. match(p,"nnv")) then
           name = names(p,2)
           value = values(p,1)
           call set_value(env,trim(name),value)
        else
           call die("Syntax error in set line")
        endif
     else if (search(p,"show",ind=ieq)) then
        if ((ieq == 1) .and. match(p,"n")) then
           call show_env(env)
        else
           call die("Syntax error in show line")
        endif
     else
        print *, "Evaluating:", "|"//trim(line)//"|" 
        call evaluate(line,env,value,status)
        IF (status > 0) then
           WRITE(*,*)'*** Error: ',EvalErrMsg (status)
        else
           WRITE(*,*) trim(line),' = ',value
        endif
        call destroy(p)
     END if
  !
  END DO
  999 continue

  CONTAINS

  subroutine die(str)
    character(len=*), intent(in) :: str

    print *, str
    stop
  end subroutine die

end PROGRAM repl
