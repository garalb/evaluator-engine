PROGRAM agtest

!  use env_type, only: eval_env_t
  use m_eval_env
!  use fparser_interface, only: evaluate, EvalErrMsg
  use fparser_interface, only: EvalErrMsg

  IMPLICIT NONE

  integer, parameter :: dp = selected_real_kind(15,100)

  REAL(dp)                                       :: res
  character(len=80) :: funstr
  integer :: status

   type(eval_env_t)  :: env
  !--------- 
  !
  call new_env(env,3)
  call add_to_eval_env(env,"x",2.0_dp)
  call add_to_eval_env(env,"y",0.5_dp)
  call add_to_eval_env_deferred(env,"zz","x-2*y")
  call add_to_eval_env(env,"pi",3.141592653584_dp,read_only=.true.)
  call show_env(env)
  DO
    print *, "Enter function string (using vars in above environment)"
    read "(a)", funstr
    print *, "Function to be evaluated: ", "|"//trim(funstr)//"|"

    call evaluate(funstr,env,res,status)
    IF (status > 0) then
       WRITE(*,*)'*** Error: ',EvalErrMsg (status)
    else
       WRITE(*,*) trim(funstr),' = ',res
    endif

  END DO
  !
END PROGRAM agtest
