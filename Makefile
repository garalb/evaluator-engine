.SUFFIXES:
.SUFFIXES: .f90 .f .o 

F90          = gfortran
F90FLAGS     = -g

DEST         = repl
BASIS        = parameters.o fparser.o evaluation.o repl.o parse.o

.f90.o: 
	$(F90) $(F90FLAGS) -c $<
.f.o: 
	$(F90) $(F90FLAGS) -c $<

repl: $(BASIS) 
	$(F90) -o $(DEST) $(F90FLAGS) $(BASIS)

clean:
	rm -fr *.o *.mod repl

repl.o: parameters.o evaluation.o fparser.o parse.o

fparser.o: parameters.o evaluation.o






